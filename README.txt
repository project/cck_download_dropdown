/**
 * @package CCK_Download_Dropdown
 * @category NeighborForge
 */
This module adds the ability to place a single drop down selector and download button
on a node for use by the viewer. The list is populated by links to attachments from a
node that you specify. Selecting an attachment and pressing the download button will
initiate a download of the selected file. Although multiple drop downs in a single
field are not supported, you can add multiple fields, each with a drop down populated
by different files.

--CONTENTS--
  REQUIREMENTS
  INSTALL/SETUP
  FEATURES
  USAGE (Example)
  UNINSTALL
  CREDITS
  HELP
  
--REQUIREMENTS--
  *************************************************************************************
  IMPORTANT!!! - You need to supply an icon, call it disk.png, and place it in this
  module's folder. The one I use wasn't GPL-compatible so could not be included in this
  release. The one I use looks like a floopy disk, but you could use anything.
  *************************************************************************************
  
  This module depends on the upload module that comes with Drupal by default. In the
  future, other file managers may be supported. For now, we're looking for attachments
  to nodes as specified by the upload module.
  
  This module also depends on the content module.
  
--INSTALL/SETUP--
  Copy entire folder and contents into your modules folder in your Drupal installation;
  usually this is sites/all/modules.
  
  Activate the module.
  
--FEATURES--
  Present users with a drop down list of attachments instead of a table.
  
  Have multiple drop downs on a single page, each with its own list of files. This allows
  you to have one drop down per file category.
  
  Order the file list either in ascending or descending order.
  
  Made more for use by admins, not end-users since there are a few caveats to beware of.
  
--USAGE--
  1) The first thing to do is to create a regular page and attach a bunch of files to it
     like normal. I suggest creating several such pages, broken down into categories. An
     example of why you would do this is if you have a ton of MP3s you want to allow users
     to download. Create one page for each category, and title it with the category name.
     Leave the body empty as you won't see it in conjunction with this module. You will
     still be able to allow users to view the pages if you want, but you could also leave
     the nodes 'unpublished' so as to be unreachable themselves.
  
  2) Create a new content type. Title it MP3 downloads for the above example. Fill in the
     body with a description of the categories that follow and maybe your own download
     instructions which paraphrase what I'll tell you below.
     
  3) Add a new field to the new content type you just created. Give it an appropriate
     machine language name, and select this module's 'cck_download_dropdown' type for the
     field. Submit the form. On the next page, title the field the same as one of your
     categories you identified (page title(s) from step 1). I suppose you could make the
     field REQUIRED, but I don't know why you would. Select which sort order you would
     like the drop down to have. This assumes that you know what files are named ahead
     of time. The default is ASCENDING. I put in the option for DESCENDING because I am
     listing files named by date such as 2007.10.06.Filename.pdf and I wanted the newest
     to be at the top.
     
  4) Repeat step 3 for each category you identified/page you created in step 1.
  
  5) Weight the fields as you wish them to show up. You may want to use the fieldgroup
     module if you have a lot of catagories (fields) so that you can ensure they are below
     the body and then ordered correctly. It seems the body is weighted at 0 and you can't
     change it, so weighting the others only gives you ten spots heavier than the body. So
     should you have more than 10 categories/drop downs, you won't be able to give them
     unique weights without the fieldgroup module.
     
  6) Create a new page using your new content type. Each field created with this module
     should present you with a drop down list of all nodes' titles where you uploaded docs
     via the upload module (i.e. have entries into the 'files' table in your database).
     If you named the category nodes by the category name, and if you named the cck field
     the category name as well, then you should just have to match the node name with the
     field name. NOTE: the use of the word 'category' has nothing to do with the taxonomy
     module - I'm using it in the real-world sense.
  
  7) Match all of the other fields to nodes, then submit the page (after filling in anything
     else you may have added as fields to the node).
     
  8) Now when you visit the page, you'll see several drop down lists of files (MP3s in our
     example), each with a download button or icon next to it. If you see the icon, then
     you have javascript enabled. This image is simply a link to the currently selected file
     in its associated drop down list. If you select a new item, the link will change. This
     allows users to download as many documents as they want without leaving the page.
     
  9) If javascript is not enabled, instead of a download icon, a regular form button will
     display. Hopefully it works, but I admit it isn't as tested as the js solution. What
     should happen is that a user selects a file from any drop down, then clicks the button.
     They will be served that document, replacing the current page if the document can be
     opened by the browser. I couldn't figure out how to get drupal_goto() to serve a page
     in a new window. I am not sure what happens when the doc can't be viewed inside the
     browser. I would imagine that the browser will ask you if you want to download via
     a popup. The page will either return to the download page, or will be blank, in which
     case the user will have to browse back. If anyone has ideas how to make this part work
     better, I'd love to hear it.
     
--UNINSTALL--
  This module does not install any of its own tables into your database, nor does it use
  its own variables in the variables table, so there is nothing to uninstall if you
  ever choose to deactivate this module.
  
--CREDITS--
  This module was created by Ryan Constantine (rconstantine) Nov 20, 2007
  
--HELP--
  Not for you, for me-
    If you have ideas for this module and especially if you can make patches,
    please do. I'm open to any improvements.
  
  For you-
    you can post questions, comments, and patches to the issue queue for this module on
    the Drupal.org web site.
